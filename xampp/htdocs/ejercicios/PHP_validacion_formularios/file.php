<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VALIDACION DE FORMULARIOS</title>
</head>
<body>

<h1>Validacion de formularios</h1>

<?php

if(isset($_GET['error'])){

$error = $_GET['error'];
if($error == 'faltanValores') {

    echo '<strong style="color:red">Introduce todos los datos en los campos del formulario</strong>';
}

}


?>

<form method="POST" action="procesarFormulario.php">

<label for="nombre">Nombre</label>
<p><input type="text" name="nombre" required="required" pattern="[A-Za-z]+"></p>
<label for="apellidos">Apellidos</label>
<p><input type="text" name="apellidos"></p>
<label for="edad">Edad</label>
<p><input type="number" name="edad" required="required" pattern="[0-9]+"></p>
<label for="email">E-mail</label>
<p><input type="email" name="email"></p>
<label for="password">Contraseña</label>
<p><input type="password" name="password" required="required" minlength="7"></p>

<input type="submit" value="Enviar" />


</form>

</body>
</html>