<?php

$error = 'faltanValores';

if(!empty($_POST['nombre']) && !empty($_POST['apellidos']) && !empty($_POST['edad']) && !empty($_POST['email']) && !empty($_POST['password'])){

$error = 'ok';
    $nombre = $_POST['nombre'];
    $apellidos = $_POST['apellidos'];
    $edad = $_POST['edad'];
    $email = $_POST['email'];
    $password = $_POST['password'];


//VALIDAR EL NOMBRE//

if(!is_string($nombre)){

    $error = 'nombre';
}


if(!is_string($apellidos)){

    $error = 'apellidos';
}

if(!is_numeric($edad) && !filter_var($edad, FILTER_VALIDATE_INT)) {

    $error = 'edad';
}

if(!is_string($email) && !filter_var($email, FILTER_SANITIZE_EMAIL))
    $error = 'email';

if(empty($password) && strlen($password)<5)

    $error = 'password';

}else{

$error = 'faltanValores';
}

if ($error != 'ok'){

    header("Location:file.php?error=$error");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Validación de formularios</title>
</head>
<body>
<?php if($error == 'ok'): ?>
    <h1>Datos validados correctamente</h1>
    <p><?=$nombre?></p>
    <p><?=$apellidos?></p>
    <p><?=$edad?></p>
    <p><?=$email?></p>
   
<?php endif; ?>

</body>
</html>